import math
class Calculador:

    def __int__(self, a, b, c, d):
        self.angulo=a
        self.coeficiente=b
        self.masa1=c
        self.masa2=d

    def calcular_movimiento(self):
        if self.masa1 > self.masa2:
            n="Subiendo"
        else:
            n="Bajando"
        return   n 
        
    def calcular_angulo(self):
        ang=math.asin(self.masa2/self.masa1)
        m=(self.masa2/self.masa1)
        x=(m*9.8*math.sin(ang)-self.coeficiente*math.cos(ang))
        if x==0:
            ret=ang
        else:
            ret=-1
        return ret

miCalculador=Calculador()
miCalculador.masa1=float(input("Ingrese el valor de la masa 1: "))
miCalculador.masa2=float(input("Ingrese el valor de la masa 2: "))
miCalculador.coeficiente=float(input("Ingrese el valor del coeficiente de friccion: "))

print("El resultado es: ",miCalculador.calcular_angulo())
print("El resultado es: ",miCalculador.calcular_movimiento())